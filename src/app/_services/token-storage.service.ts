import { Injectable } from '@angular/core';
import { User } from '../model/user';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  signOut() {
    window.sessionStorage.clear();
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): String {
    return sessionStorage.getItem(TOKEN_KEY)|| '';
  }

  public saveUser(user:User) {
    console.log("user",user);
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser() {
    console.log("sessionStorage.getItem(USER_KEY)",sessionStorage.getItem(USER_KEY));

    return JSON.parse(sessionStorage.getItem(USER_KEY)|| '{}');
  }
}