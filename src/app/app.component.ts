import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'websolution';
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username!: string;
  constructor(private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
    console.log("this.tokenStorage.getUser()",this.tokenStorageService.getUser());
      console.log("this.tokenStorage.getToken()",this.tokenStorageService.getToken());
      console.log("this.tokenStorage.()",this.tokenStorageService);
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.username = user.username;
    }
  }

  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
