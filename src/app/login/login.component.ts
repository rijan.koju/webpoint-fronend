import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  username='';

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {

    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      console.log("this.tokenStorage.getUser()",this.tokenStorage.getUser());
      console.log("this.tokenStorage.getToken()",this.tokenStorage.getToken());
      console.log("this.tokenStorage.()",this.tokenStorage);


      this.username = this.tokenStorage.getUser().username;
    }else{
      console.log("this.tokenStorage.()",this.tokenStorage);

    }
  }

  onSubmit() {
    this.authService.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.entity.token);
        this.tokenStorage.saveUser(data.entity.userDto);

        this.isLoggedIn = true;
        console.log("this.tokenStorage.getUser()",this.tokenStorage.getUser());
        this.username = this.tokenStorage.getUser().username;
        this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage() {
    window.location.reload();
  }
}